const WHITELIST = []; // ["http://test.ts:3000"];

const checkWhitelisted = (origin) => WHITELIST.find((o) => o === origin);

const corsMiddleware = (req, res, next) => {
  console.log("corsMiddleware: Origin: ", req.headers.origin);

  const origin = checkWhitelisted(req.headers.origin);

  if (origin) {
    res.header("Access-Control-Allow-Origin", origin);
  }

  res.set({
    "Access-Control-Allow-Credentials": false,
    "Access-Control-Allow-Methods": "GET, POST, DELETE",
    "Access-Control-Allow-Headers": "Content-Type, X-Custom-Header",
    "Access-Control-Expose-Headers": "Content-Type, X-Custom-Header",
  });

  if (req.method === "OPTIONS") {
    console.log("corsMiddleware: OPTIONS request");

    res.header("Access-Control-Max-Age", 1);
    return res.status(origin ? 200 : 403).end();
  }

  next();
};

module.exports = { corsMiddleware };
