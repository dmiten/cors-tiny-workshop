const express = require("express");
const path = require("node:path");

const { corsMiddleware } = require("./cors.middleware");

const PORT = process.env.PORT || 3000;
const app = express();

// app.use(corsMiddleware)

app.use(corsMiddleware);
app.use(express.static(path.join(__dirname, "public")));

app.get("/cors", (req, res, next) => {
  res.send("GET /cors: ok");
});

app.post("/cors", (req, res, next) => {
  res.send("POST /cors: ok");
});

app.delete("/cors", (req, res, next) => {
  res.send("DELETE /cors: ok");
});

app.listen(PORT, () => {
  console.log(`CORS-enabled web server listening on port ${PORT}`);
});
